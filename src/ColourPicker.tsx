function ColourPicker(props: {
  defaultValue: string;
  onChange: React.Dispatch<React.SetStateAction<string>>;
}) {
  return (
    <form>
      <fieldset>
        <legend>Colour Picker</legend>
        <label htmlFor="target">Target</label>
        <input
          onChange={(e) => {
            props.onChange(e.target.value);
          }}
          type="color"
          id="target"
          name="target"
          defaultValue={props.defaultValue}
        />
      </fieldset>
    </form>
  );
}

export default ColourPicker;
