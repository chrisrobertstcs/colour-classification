import Colour from "./Colour";

// ref: https://en.wikipedia.org/wiki/Web_colors#Basic_colors
const map: Map<string, string> = new Map();
map.set("White", "#ffffff");
map.set("Silver", "#c0c0c0");
map.set("Gray", "#808080");
map.set("Black", "#000000");
map.set("Red", "#ff0000");
map.set("Maroon", "#800000");
map.set("Yellow", "#ffff00");
map.set("Olive", "#808000");
map.set("Lime", "#00ff00");
map.set("Green", "#008000");
map.set("Aqua", "#00ffff");
map.set("Teal", "#008080");
map.set("Blue", "#0000ff");
map.set("Navy", "#000080");
map.set("Fuchsia", "#ff00ff");
map.set("Purple", "#800080");

const nameColourMap: Map<string, Colour> = new Map();
map.forEach((colour, name) => {
  nameColourMap.set(name, new Colour(colour));
});

export default nameColourMap;
