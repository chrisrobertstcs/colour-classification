import { useState } from "react";
import "./App.css";
import Colour from "./Colour";
import ColourPicker from "./ColourPicker";
import DistancesTable from "./DistancesTable";

function App() {
  const [target, setTarget] = useState(
    "#" +
      Math.floor(Math.random() * (0xffffff + 1)) // <= 0xffffff
        .toString(16) // convert to hex
        .padStart(6, "0")
  ); // #rrggbb

  return (
    <>
      <header>
        <h1>Closest Colour</h1>
      </header>
      <main>
        <p>
          A demonstration of using a colour's red, blue and green values as
          spacial co-ordinates.
        </p>
        <ColourPicker defaultValue={target} onChange={setTarget} />
        <DistancesTable target={new Colour(target)} />
      </main>
      <footer>
        <a href="/" title="Click to reload.">
          ⓘ Reloading the page generates a random target.
        </a>
      </footer>      
    </>    
  );
}

export default App;
