import Colour from "./Colour";
import nameColourMap from "./nameColourMap";

function nameDistanceMapTable(props: { target: Colour }) {
  const nameDistanceMap: Map<string, number> = new Map();
  nameColourMap.forEach((colour, name) => {
    nameDistanceMap.set(name, props.target.getDistance(colour));
  });

  return (
    <table className="nameDistanceMapTable">
      <thead>
        <tr>
          <th>Colour</th>
          <th>Name</th>
          <th>x</th>
          <th>y</th>
          <th>z</th>
          <th>Distance to Target <abbr title="In accending order.">↓</abbr></th>
        </tr>
      </thead>
      <tbody>
        <tr key="target">
          <td style={{ background: props.target.getRGB() }}></td>
          <td>Target</td>
          <td>{props.target.getRed()}</td>
          <td>{props.target.getGreen()}</td>
          <td>{props.target.getBlue()}</td>
          <td>0</td>
        </tr>
        {[...nameDistanceMap.entries()]
          .sort((a, b) => a[1] - b[1]) // shortest distance first
          .map((item, index) => {
            const name = item[0];
            const colour = nameColourMap.get(name);
            const distance = item[1];

            return (
              <tr key={index}>
                <td style={{ background: colour?.getRGB() }}></td>
                <td>{name}</td>
                <td>{colour?.getRed()}</td>
                <td>{colour?.getGreen()}</td>
                <td>{colour?.getBlue()}</td>
                <td>{distance.toFixed(12)}</td>
              </tr>
            );
          })}
      </tbody>
    </table>
  );
}

export default nameDistanceMapTable;
