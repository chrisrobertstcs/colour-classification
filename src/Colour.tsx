import assert from "assert";

class Colour {
  private rgb: string; // '#rrggbb'
  private red: number;
  private green: number;
  private blue: number;

  constructor(colour: string) {
    const regex = new RegExp("^#[0-9a-fA-F]{6}$");
    assert(regex.test(colour));
    this.rgb = colour;
    this.red = parseInt(colour.substring(1, 3), 16);
    this.green = parseInt(colour.substring(3, 5), 16);
    this.blue = parseInt(colour.substring(5, 7), 16);
  }

  public getRGB() {
    return this.rgb;
  }

  public getRed() {
    return this.red;
  }

  public getGreen() {
    return this.green;
  }

  public getBlue() {
    return this.blue;
  }

  // ref: https://en.wikipedia.org/wiki/Euclidean_distance#Higher_dimensions
  // using the red, blue and green values as spacial co-ordinates
  public getDistance(B: Colour) {
    let A = this;
    return Math.sqrt(
      Math.pow(A.getRed() - B.getRed(), 2) +
        Math.pow(A.getGreen() - B.getGreen(), 2) +
        Math.pow(A.getBlue() - B.getBlue(), 2)
    );
  }

  public toString() {
    return (
      "Colour { rgb: " +
      this.rgb +
      ", red: " +
      this.red +
      ",  green: " +
      this.green +
      ", blue: " +
      this.blue +
      " }"
    );
  }
}

export default Colour;
